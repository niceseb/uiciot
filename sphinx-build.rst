Sphinx-Build and Documentation Update
===============
.. That has a paragraph about a main subject and is set when the '='
.. is at least the same length of the title itself.




This document is located in index.rst:

/Users/scheung/git/UICIoT/doc/index.rst

I used atom as an editor but user can user anything as needed.


To start a new sphinx document:

Make sure Python is installed:

$ which python

/Users/scheung/anaconda/envs/venv/bin/python

$ python --version

Python 2.7.10 :: Anaconda 2.3.0 (x86_64)

Then:

$ pip install Sphinx

$ sphinx-quickstart

$ sphinx-build -b html . ./builddir

But if the sphinx-quickstart build procedure is followed and MakeBuild [Y]
then it is easier to do 'make html' than the above 'sphinx-build -b html . ./builddir'

$ make html

Then navigate to the

/Users/scheung/git/UICIoT/doc/_build/html directory and right click on index.html to open generated html in Chrome
if successful then you will get:

make html

sphinx-build -b html -d _build/doctrees   . _build/html

Running Sphinx v1.3.1

loading pickled environment... done

building [mo]: targets for 0 po files that are out of date

building [html]: targets for 0 source files that are out of date

updating environment: 0 added, 0 changed, 0 removed

looking for now-outdated files... none found

no targets are out of date.

build succeeded.

Build finished. The HTML pages are in _build/html.

(venv)localhost:doc scheung$

To open in Chrome, the generated index.html is in:

Users/scheung/git/UICIoT/doc/_build/html/index.html


.. Subject Subtitle
.. ----------------
.. Subtitles are set with '-' and are required to have the same length
.. of the subtitle itself, just like titles.

.. Lists can be unnumbered like:

.. * Item Foo
.. * Item Bar

.. Or automatically numbered:

.. #. Item 1
.. #. Item 2

.. Inline Markup
.. -------------
.. Words can have *emphasis in italics* or be **bold** and you can define
.. code samples with back quotes, like when you talk about a command: ``sudo``
.. gives you super user powers!
