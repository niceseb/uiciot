RHT03
=====

https://dlnmh9ip6v2uc.cloudfront.net/images/products/1/0/1/6/7/10167-01_i_ma.jpg
*RHT03 Humidity and Temperature Sensor(SEN-10167)*](https://www.sparkfun.com/products/10167)

The RHT03 (also known as the DHT-22) is a low cost humidity and temperature sensor with a single wire digital interface.
