Introduction to IoT
===============
.. That has a paragraph about a main subject and is set when the '='
.. is at least the same length of the title itself.


Short description of the circuit setup.

Originally idea was to measure and acquire temperature, relative humidity, timestamp and other
data, saved to database (mongodB should be a good choice) and visualize data over time using Pinoccio board and Arduino IDE (1.6.5)
and using database storage like mongodb, and visualize (mongolab, parse or plotly, carriot). But due to time constraint
only temperature and humidity measurements have been measured as there were a variety of setup and configuration needed from
software as well as hardware power issue for the RHT03 one wire PWM sensor as it may need 5V as opposed to the 3.3V that's supplied
by the Pinoccio April board.

Once user signs up with Electric Imp, code build and run is available using their URL, for example
https://ide.electricimp.com/ide/models/49142/devices/23227a3643fc42ee

This link demostrates usage of DHT22, which is equivalent to the RHT03
https://github.com/electricimp/reference/blob/master/hardware/DHT22/dht22.device.nut

Potential Issues:

Reported that Pinoccio April 3.3V board may not be able to provide sufficient power for the
RHT03 sensor as it doesn't work well and should use 5V, and hence this could be the reason
why the Pinoccio board setup, using the Arduino IDE software wasn't getting any readings? Whereas the Arduino Uno board had more success?

This should be a good link for data storage and visualization using Plotly:
http://adilmoujahid.com/posts/2015/07/practical-introduction-iot-arduino-nodejs-plotly/
https://plot.ly/electric-imp/tmp36-temperature-tutorial/


Further links

Datasheet for Sensor DHT11

https://www.sparkfun.com/products/10167
https://github.com/sparkfun/RHT03/blob/master/Firmware/RHT03_Example/RHT03_Example.ino
http://akizukidenshi.com/download/ds/aosong/DHT11.pdf

Link to live temperature measurement

https://raw.githubusercontent.com/plotly/workshop/master/electric-imp/tmp36/tmp36-agent.nut
https://raw.githubusercontent.com/plotly/workshop/master/electric-imp/tmp36/tmp36-device.nut
https://github.com/electricimp/mongolab/tree/v2.0.0
https://ide.electricimp.com/ide/models/49142/devices/23227a3643fc42ee
https://forums.electricimp.com/discussion/3953/how-to-save-temperature-data-from-temperature-sensor-into-database
http://arduino-info.wikispaces.com/TemperatureHumidity
http://garagelab.com/profiles/blogs/tutorial-humidity-and-temperature-sensor-with-arduino
http://tso.gr/
http://codepen.io/jingman/pen/zqEfk/
https://electricimp.com/businesssolutions/partners/

MongoLab

Create your own account
http://docs.mongolab.com/
https://electricimp.com/docs/libraries/webservices/mongolab.2.0.0/
https://electricimp.com/docs/libraries/webservices/mongolab.2.0.0/
https://github.com/electricimp/mongolab/tree/v2.0.0
https://mongolab.com/databases/test_uiciot

Plotly
https://github.com/plotly/workshop/tree/master/electric-imp/tmp36

Carriots

How to connect a Electric Imp to Carriots and build an Alert System:
https://www.carriots.com/tutorials/electric_imp_carriots/alert_system

https://github.com/mclyde/HousePlantMonitor

Twitter API Key:

https://apps.twitter.com/app/9014841/keys

Sphinx doc:

http://sphinx-doc.org/latest/tutorial.html
http://www.ibm.com/developerworks/library/os-sphinx-documentation/
https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst
http://blog.teracy.com/2013/11/26/sphinx-deployment-v0-3-0-with-heroku-deployment-support/


The DHTester setup
Open the DJTTest.ino in

/Users/scheung/git/UICIoT/Arduino Examples/DHTtester

Board: Arduino/Genuino Uno

Port: /dev/cv/usbmodem1461 (Arduino/Genuino Uno)

Programmer: /ArduinoISP/AVRISPmkII/USBtinyISP, Parallel Programmer




.. Subject Subtitle
.. ----------------
.. Subtitles are set with '-' and are required to have the same length
.. of the subtitle itself, just like titles.

.. Lists can be unnumbered like:

.. * Item Foo
.. * Item Bar

.. Or automatically numbered:

.. #. Item 1
.. #. Item 2

.. Inline Markup
.. -------------
.. Words can have *emphasis in italics* or be **bold** and you can define
.. code samples with back quotes, like when you talk about a command: ``sudo``
.. gives you super user powers!
