MyNodejsSetup
=====

Nodejs is needed if we want to setup Pinoccio server and running pinoccio on command line.

.bashrc
alias sa='source activate venv'
alias sd='source deactivate'
alias sb='source ~/.bashrc'

# activate nvm shell
# . ~/.nvm/nvm.sh

# nvm install 0.10.35
#this installs node 0.10.35 for Maverick

# nvm alias default stable
# nvm deactivate

export PATH=/usr/local/bin:$PATH

export PATH=/usr/local/Cellar/mongodb/3.0.7/bin:$PATH
