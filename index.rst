.. UICIoT documentation master file, created by
   sphinx-quickstart on Fri Oct 30 13:04:02 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UICIoT's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   introduction

   sphinx-build

   RHT03-Sensor

   MyNodejs-Setup

   MongodB-Setup

   Run-Your-Own-Pinoccio-Server

   Create-a-new-Troop-via-CLI



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
