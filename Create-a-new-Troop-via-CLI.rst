Create-a-new-Troop-via-CLI
======================
pinoccio login
wifi.config("FCC M&P","Futur3m&p!");
wifi.dhcp;
wifi.reassociate;



//Create a new Troop and make note of the token and id it returns - you'll need them later (we'll reference them as TROOP_ID and TROOP_TOKEN from now on).

$ pinoccio rest post v1/troop --name "UICIoT Troop" //was Sesame Street
{
    "id": 4,
    "token": "5912c1a21631f568dee306f70a8d9fed",
    "account": 3559
}

$ pinoccio rest post v1/TROOP_ID/scout --name "UICIoT Scout"
$ pinoccio rest post v1/4/scout --name "UICIoT Scout" //was "Big Bird"
{
    "name": "UICIoT Scout",
    "updated": "1445967719808",
    "id": 1,
    "time": 1445967719807
}

$ pinoccio serial

Expecting: You should see a "Hello Pinoccio" prompt.
Actual: Error: bitlash: did not find prompt after serial was idle for over 20 seconds.
    at fn [as _onTimeout] (/Users/scheung/.nvm/v0.10.35/lib/node_modules/pinoccio/node_modules/pinoccio-serial/index.js:75:16)
    at Timer.unrefdHandle [as ontimeout] (timers.js:295:14)


$ hq.settoken(TROOP_TOKEN);
$ mesh.config(SCOUT_ID, TROOP_ID, 20);
$ mesh.setkey(MESH_KEY); // MESH_KEY any string between 0 and 16 characters.

$ hq.settoken(5912c1a21631f568dee306f70a8d9fed); //syntax error unexpected token
$ hq.settoken("5912c1a21631f568dee306f70a8d9fed"); //syntax error unexpected token
$ mesh.config(1, 4, 20);
$ mesh.setkey("Hhpkmvq46!");



LOGIN TO HQ
curl -X POST -v --data 'email=sebastian_cheung@yahoo.com&password=linear11' https://api.pinocc.io/v1/login

CREATING A READ-ONLY TOKEN
curl -X POST https://api.pinocc.io/v1/account/token?token=1rfjppnm89atq8po6um36osmu0
