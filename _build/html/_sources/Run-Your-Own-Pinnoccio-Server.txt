RUN YOUR OWN PINOCCIO SERVER


Arduino IDE
include
/Users/scheung/Downloads

http://support.pinocc.io/hc/en-us/articles/202958674-Create-a-New-Troop-via-the-CLI
TOKENID = 1rfjppnm89atq8po6um36osmu0

First MUST activate node version using nvm:
. ~/.nvm/nvm.sh
$nvm install 0.10.35 (for Maverick) //default which node -> v0.12.7

$ npm install -g pinoccio
$ mkdir myUICIoT
$ cd myUICIoT

Install the Pinoccio server module:
$ npm install --save pinoccio-server

pinoccio-server@0.1.1 node_modules/pinoccio-server
├── through@2.3.8
├── split@0.3.3
└── reconnect-net@0.0.0 (reconnect-core@0.0.1)

Create a server script:
/Users/scheung/myUICIoT/server.js:

Start the server:
$ node server.js &
local pinoccio server listening on  { address: '0.0.0.0', family: 'IPv4', port: 22756 }

Connect your Troop to your server:
Option 1: Using Bridge Mode
$ pinoccio bridge -v --host 127.0.0.1

scanning for scouts.
<parent> plug undefined
device plugged in! { comName: '/dev/cu.Bluetooth-Incoming-Port',
  manufacturer: '',
  serialNumber: '',
  pnpId: undefined,
  locationId: '',
  vendorId: '',
  productId: '',
  _id: '/dev/cu.Bluetooth-Incoming-Port||' }
<parent> plug undefined
device plugged in! { comName: '/dev/cu.Bluetooth-Modem',
  manufacturer: '',
  serialNumber: '',
  pnpId: undefined,
  locationId: '',
  vendorId: '',
  productId: '',
  _id: '/dev/cu.Bluetooth-Modem||' }
<parent> plug 953363337353519192D1
Starting bridge worker.
Worker being forked with uid 1593836940 and gid 20
device plugged in! { comName: '/dev/cu.usbmodem1461',
  manufacturer: 'Pinoccio',
  serialNumber: '953363337353519192D1',
  pnpId: '953363337353519192D1',
  locationId: '0x14600000',
  vendorId: '0x1d50',
  productId: '0x6051',
  _id: '/dev/cu.usbmodem1461|Pinoccio|953363337353519192D1' }
bridging with options  { host: '127.0.0.1' }
Building bridge on  /dev/cu.usbmodem1461
verbose!




Option 2: Using a WiFi Lead Scout
$ hq.setaddress("YOUR_IP_ADDRESS"); wifi.dhcp; wifi.reassociate;
$ hq.setaddress("172.16.12.110"); wifi.dhcp; wifi.reassociate; //syntax error
$ hq.setaddress(172.16.12.110); wifi.dhcp; wifi.reassociate; //syntax error
$ hq.setaddress('172.16.12.110'); wifi.dhcp; wifi.reassociate; //syntax error

> hq.setaddress("172.16.12.110"); wifi.dhcp; wifi.reassociate; //hq is not defined
> hq.setaddress('172.16.12.110'); wifi.dhcp; wifi.reassociate; //hq is not defined
> hq.setaddress(172.16.12.110); wifi.dhcp; wifi.reassociate; //SyntaxError: Unexpected number
